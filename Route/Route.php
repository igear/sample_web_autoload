<?php

namespace Route;



class Route
{

	public function toController($file)
	{
		$fileController="Controller\\".$file.'Controller';
		$GetController=New $fileController();
	}
	public function Routing()
	{
		if(isset($_GET['page']))
		{
			$this->toController('page');
		}
		else
		{
			$this->toController('home');
		}
	}
}

?>